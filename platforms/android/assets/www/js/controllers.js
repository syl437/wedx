angular.module('starter.controllers', [])

.controller('AppCtrl', function($scope, $ionicModal, $timeout,$state,$localStorage,$rootScope) {

  // With the new view caching in Ionic, Controllers are only called
  // when they are recreated or on app start, instead of every page change.
  // To listen for when this page is active (for example, to refresh data),
  // listen for the $ionicView.enter event:
  //$scope.$on('$ionicView.enter', function(e) {
  //});
	
  // Form data for the login modal
  $scope.loginData = {};

  // Create the login modal that we will use later
  $ionicModal.fromTemplateUrl('templates/login.html', {
    scope: $scope
  }).then(function(modal) {
    $scope.modal = modal;
  });

  // Triggered in the login modal to close it
  $scope.closeLogin = function() {
    $scope.modal.hide();
  };

  // Open the login modal
  $scope.login = function() {
    $scope.modal.show();
  };

  // Perform the login action when the user submits the login form
  $scope.doLogin = function() {
    console.log('Doing login', $scope.loginData);

    // Simulate a login delay. Remove this and replace with your login
    // code if using a login system
    $timeout(function() {
      $scope.closeLogin();
    }, 1000);
  };
  
	$scope.getSelectedPrice = function(deal,guests)
	{

		var price = 0;
		if (parseInt(guests) < 200 )
		{
			price = deal.price200;
		}
		else if (parseInt(guests) < 350 )
		{
			price = deal.price350;
		}					
		else if (parseInt(guests) < 500 )
		{
			price = deal.price500;
		}
		else
		{
			price = deal.priceabove500;
		}					
		return price;
	}  
  $scope.LogOut = function()
  {
	  $localStorage.userid = '';
	  $localStorage.name = '';
	  $localStorage.email = '';
	  $state.go('app.login');
  }

  
})


.controller('LoginCtrl', function($scope, $stateParams,$ionicPopup,$http,$rootScope,$state,$localStorage,$ionicSideMenuDelegate,$ionicNavBarDelegate,$ionicModal,$ionicHistory) {

 //$ionicNavBarDelegate.show(false);

$ionicHistory.nextViewOptions({
	disableAnimate: true
});

$rootScope.ControllerName = "login";
			
if ($localStorage.userid)
{
	$state.go('app.main');
}

$scope.login = 
{
	"email" : "",
	"password" : ""
}

$scope.forgot = 
{
	"email" : ""
}

$scope.LoginBtn  = function()
{
	$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';
    emailRegex = /\S+@\S+\.\S+/;
    

	if ($scope.login.email =="")
	{
			$ionicPopup.alert({
			 title: 'יש להזין כתובת דוא"ל',
			buttons: [{
				text: 'אשר',
				type: 'button-positive',
			  }]
		   });
	}
	else if (!emailRegex.test($scope.login.email))
	{
			$ionicPopup.alert({
			 title: 'כתובת דוא"ל לא תקינה נא לתקן',
			buttons: [{
				text: 'אשר',
				type: 'button-positive',
			  }]
		   });
	}	
	else if ($scope.login.password =="")
	{
			$ionicPopup.alert({
			 title: 'יש להזין סיסמא',
			buttons: [{
				text: 'אשר',
				type: 'button-positive',
			  }]
		   });
	}	
	else
	{
		login_data = 
		{
			"email" : $scope.login.email,
			"password" : $scope.login.password
		}					
		$http.post($rootScope.Host+'/login.php',login_data).success(function(data)
		{
			if (data.response.status == 0)
			{
				$ionicPopup.alert({
				 title: 'כתובת דוא"ל או סיסמה שגוים נא לתקן',
				buttons: [{
					text: 'אשר',
					type: 'button-positive',
				  }]
			   });					
			}
			else
			{
				$localStorage.userid = data.response.userid;
				$localStorage.name = data.response.name;
				$localStorage.email = data.response.email;
				$state.go('app.main');
			}
		});		
	}
}

	$scope.ForgotPass = function()
	{
	  $ionicModal.fromTemplateUrl('templates/forgot_pass.html', {
		scope: $scope
	  }).then(function(ForgotPassModal) {
		$scope.ForgotPassModal = ForgotPassModal;
		$scope.ForgotPassModal.show();
	  });
	}
	$scope.closeModal = function()
	{
		$scope.ForgotPassModal.hide();
	}
	
	
	
	$scope.sendPasswordBtn = function()
	{
		$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';
		emailRegex = /\S+@\S+\.\S+/;
		

		if ($scope.forgot.email =="")
		{
				$ionicPopup.alert({
				 title: 'יש להזין כתובת דוא"ל',
				buttons: [{
					text: 'אשר',
					type: 'button-positive',
				  }]
			   });
		}
		else if (!emailRegex.test($scope.forgot.email))
		{
				$ionicPopup.alert({
				 title: 'כתובת דוא"ל לא תקינה נא לתקן',
				buttons: [{
					text: 'אשר',
					type: 'button-positive',
				  }]
			   });
		}	
		else
		{
			
			
		send_data = 
		{
			"email" : $scope.forgot.email

		}					
		$http.post($rootScope.Host+'/send_pass.php',send_data).success(function(data)
		{
			if (data.response.status == 0)
			{
				$ionicPopup.alert({
				 title: 'לא נמצא משתמש המשויך לכתובת מייל שהזנת יש לנסות שוב',
				buttons: [{
					text: 'אשר',
					type: 'button-positive',
				  }]
			   });				
			}
			else
			{
				$ionicPopup.alert({
				 title: 'שם משתמש וסיסמה נשלחו למייל שהזנת',
				buttons: [{
					text: 'אשר',
					type: 'button-positive',
				  }]
			   });				
			}

		});	

			$scope.forgot.email = '';
			$scope.ForgotPassModal.hide();
			

		}
	}
	
	



})


.controller('RegisterCtrl', function($scope, $stateParams,$ionicPopup,$http,$rootScope,$state,$localStorage,$ionicSideMenuDelegate,$ionicHistory) {

$ionicHistory.nextViewOptions({
	disableAnimate: true
});

$rootScope.ControllerName = "register";


if ($localStorage.userid)
{
	$state.go('app.main');
}

$scope.register = 
{
	"name" : "",
	"email" : "",
	"password" : ""
}

$scope.RegisterBtn = function()
{
	$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';
    emailRegex = /\S+@\S+\.\S+/;
    
	if ($scope.register.name =="")
	{
			$ionicPopup.alert({
			 title: 'יש להזין שם מלא',
			buttons: [{
				text: 'אשר',
				type: 'button-positive',
			  }]
		   });
	}	
	
	else if ($scope.register.email =="")
	{
			$ionicPopup.alert({
			 title: 'יש להזין כתובת דוא"ל',
			buttons: [{
				text: 'אשר',
				type: 'button-positive',
			  }]
		   });
	}
	else if (!emailRegex.test($scope.register.email))
	{
			$ionicPopup.alert({
			 title: 'כתובת דוא"ל לא תקינה נא לתקן',
			buttons: [{
				text: 'אשר',
				type: 'button-positive',
			  }]
		   });
	}	
	else if ($scope.register.password =="")
	{
			$ionicPopup.alert({
			 title: 'יש להזין סיסמא',
			buttons: [{
				text: 'אשר',
				type: 'button-positive',
			  }]
		   });
	}
	else
	{
		register_data = 
		{
			"name" : $scope.register.name,
			"email" : $scope.register.email,
			"password" : $scope.register.password
		}					
		$http.post($rootScope.Host+'/register.php',register_data).success(function(data)
		{
			if (data.response.status == 0)
			{
				$ionicPopup.alert({
				 title: 'כתובת דוא"ל שהוזנה כבר בשימוש יש לבחור כתובת אחרת או להתחבר',
				buttons: [{
					text: 'אשר',
					type: 'button-positive',
				  }]
			   });					
			}
			else
			{
				$localStorage.userid = data.response.userid;
				$localStorage.name = $scope.login.email;
				$localStorage.email = $scope.login.email;
				$state.go('app.main');
			}
		});			
	}
}


})


.controller('MainCtrl', function($scope, $stateParams,$ionicPopup,$http,$rootScope,$state,$localStorage,$ionicSideMenuDelegate,$ionicModal,$timeout) 
{
//	$scope.$on('$ionicView.enter', function(e) {
  
	$scope.navTitle='<img class="title-image" src="img/logo.png" />';
	$rootScope.ControllerName = "main";


	
	$scope.agent = 
	{
		"name" : $localStorage.name,
		"email" : $localStorage.email,
		"phone" : "",
		"event" : "מקום לאירוע",
		"area" : "הכל",
		"budget" : "100",
		"guests" : "",
		"details" : ""
	}
	
	$scope.messages = 
	{
		"count" : 0
	}
	
	$scope.MessageCount = 0;
	
	$scope.increment = function(current)
	{
		//$scope.messages.count = parseInt(current)+1;
		//$scope.messages.count = 8;
		$scope.MessageCount++;
	}
	
	
	
    var channel = $rootScope.pusher.subscribe('wedx_messages');
    channel.bind($localStorage.userid, function(data) {
		$timeout(function() { 
		  if (data.status)
		  {
			  $scope.increment($scope.messages.count);
			  //$scope.getChatHistory();	
			 // alert (data.status);
		  }
		}, 1000);
    });
	
	$scope.SmartAgentModal = function()
	{
	  $ionicModal.fromTemplateUrl('templates/agent.html', {
		scope: $scope
	  }).then(function(AgentModal) {
		$scope.AgentModal = AgentModal;
		$scope.AgentModal.show();
	  });
	}
	$scope.closeAgent = function()
	{
		$scope.AgentModal.hide();
	}
	
	$scope.SendDetailsBtn = function()
	{
	$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';

		if ($scope.agent.name =="")
		{
			$ionicPopup.alert({
			 title: 'יש למלא שם מלא',
			buttons: [{
				text: 'אשר',
				type: 'button-positive',
			  }]
		   });
		}
		else if ($scope.agent.email =="")
		{
			$ionicPopup.alert({
			 title: 'יש למלא כתובת מייל',
			buttons: [{
				text: 'אשר',
				type: 'button-positive',
			  }]
		   });			
		}
		else if ($scope.agent.phone =="")
		{
			$ionicPopup.alert({
			 title: 'יש למלא מספר טלפון',
			buttons: [{
				text: 'אשר',
				type: 'button-positive',
			  }]
		   });			
		}
		else if ($scope.agent.event =="")
		{
			$ionicPopup.alert({
			 title: 'יש לבחור סוג אירוע',
			buttons: [{
				text: 'אשר',
				type: 'button-positive',
			  }]
		   });			
		}
		else if ($scope.agent.details =="")
		{
			$ionicPopup.alert({
			 title: 'יש למלא תוכן הפנייה',
			buttons: [{
				text: 'אשר',
				type: 'button-positive',
			  }]
		   });			
		}
		else
		{
				send_params = 
				{
					"user" : $localStorage.userid,
					"name" : $scope.agent.name,
					"email" : $scope.agent.email,
					"phone" : $scope.agent.phone,
					"event" : $scope.agent.event,
					"area" : $scope.agent.area,
					"budget" : $scope.agent.budget,
					"guests" : $scope.agent.guests,
					"details" : $scope.agent.details,
					"send" : 1 
				}
				$http.post($rootScope.Host+'/send_agent.php',send_params)
				.success(function(data, status, headers, config)
				{
					$scope.agent.phone = '';
					//$scope.agent.event = '';
					$scope.agent.details = '';

				})
				.error(function(data, status, headers, config)
				{

				});	
				
				$ionicPopup.alert({
				 title: 'תודה! נציג שלנו ישוב אליך תוך 24 שעות עם הצעות רלוונטיות מספקים מובילים',
				buttons: [{
					text: 'אשר',
					type: 'button-positive',
				  }]
			   });	
			   
			   $scope.AgentModal.hide();
		   
		}
		
	}
	
	$scope.goToChat = function()
	{
		//$state.go('app.chat', {  SupplierIndex: 1,UserId: 1 });
		//window.location.href = "#/app/chat/8/1";
		//$state.go('app.messages');
		$ionicSideMenuDelegate.toggleRight();
		
	}
	
	
	
//	});
})


.controller('CategoriesCtrl', function($scope, $stateParams,$ionicPopup,$http,$rootScope,$state,$localStorage,$ionicSideMenuDelegate) 
{
	$scope.navTitle='<img class="title-image" src="img/logo.png" />';
	$rootScope.ControllerName = "catagories";


	$scope.navigateUrl = function (Path,Num) 
	{
		window.location.href = Path+String(Num);
	}	
})


.controller('SupllierCtrl', function($scope, $stateParams,$ionicPopup,$http,$rootScope,$state,$localStorage,$ionicSideMenuDelegate,$ionicModal) 
{
//	$scope.$on('$ionicView.enter', function(e) 
//  	{
		//setTimeout(function()
		//{
			$scope.ItemId = $stateParams.ItemId;

			if ($scope.ItemId == 0)
			$scope.imageCatagory = "img/catagories/h2.png";
			if ($scope.ItemId == 1)
			$scope.imageCatagory = "img/catagories/h1.png";
			if ($scope.ItemId == 2)
			$scope.imageCatagory = "img/catagories/h4.png";		
			if ($scope.ItemId == 4)
			$scope.imageCatagory = "img/catagories/h3.png";


			$rootScope.ControllerName = "suppliers";

			
			if ($scope.ItemId == 2 || $scope.ItemId == 0)
			{
				$scope.showSearch = true;
			}
			else
			{
				$scope.showSearch = false;
			}
			
			$scope.search = 
			{
				"area" : "0",
				"guests" : ""
				
			}
			
			$scope.navTitle='<img class="title-image" src="img/logo.png" />'
			$scope.CatId = $stateParams.ItemId;
			$scope.CatagoryIndex = $rootScope.Data[$scope.CatId].index;
			$scope.Suppliers = $rootScope.Data[$scope.CatId].suppliers;
			
			$scope.SuppliersIndex = $rootScope.Data[$scope.CatId].index;
			
			$scope.Catagoryname = $rootScope.Data[$scope.CatId].name;
			$rootScope.Catagoryname = $scope.Catagoryname;
			//console.log($scope.Suppliers)			
			$scope.host = $rootScope.Host;
			$scope.emptyStar = 'img/suppliers/estar.png'
			$scope.fullStar = 'img/suppliers/fstar.png'
			$scope.starImg = $scope.emptyStar;
			

			$scope.checkFavorite = function()
			{
				//alert (444);
			}
			$scope.addFavorite = function(index)
			{
				$scope.favParams = 
				{
					"user" : $localStorage.userid,
					"article" : $scope.Suppliers[index].index,
					"is_deal" : 0
				}
				
				$rootScope.likedArticle.push($scope.favParams);	
				console.log('new favorite: ',$rootScope.likedArticle)
				
				if ($scope.Suppliers[index].favorite == 0)
				{
					$scope.Suppliers[index].favorite = 1;
				}
				else
				{
					$scope.Suppliers[index].favorite = 0;
				}
				
				
				send_params = 
				{
						"user" : $localStorage.userid,
						"article" : $scope.Suppliers[index].index,
						"is_deal" : 0
				}
				//console.log(chat_params)
				$http.post($rootScope.Host+'/add_favorite.php',send_params)
				.success(function(data, status, headers, config)
				{
						$ionicPopup.alert({
						 title: 'נוסף בהצלחה למועדפים',
						buttons: [{
							text: 'אשר',
							type: 'button-positive',
						  }]
					   });
				})
				.error(function(data, status, headers, config)
				{

				});	

			}
			
			
			$scope.openChat = function(index,id)
			{
				//alert (id);
				//$state.go('app.chat', { CatIndex: $scope.CatId, SupplierIndex: index });
				//$state.go('app.chat', {  SupplierIndex: id });
				
				$state.go('app.chat', { SupplierIndex: id, UserId: 4 });
			}
			$scope.SearchSupp = function()
			{
			  $ionicModal.fromTemplateUrl('templates/supplier_search.html', {
				scope: $scope
			  }).then(function(supplierSearchModal) {
				$scope.supplierSearchModal = supplierSearchModal;
				$scope.supplierSearchModal.show();
			  });
			}

			$scope.closeSearch = function()
			{
				$scope.supplierSearchModal.hide();
			}
			
			$scope.navigateSearch = function(id)
			{
				$scope.supplierSearchModal.hide();
				$state.go('app.info', { ArticleId: id });
			}

			
			$scope.searchBtn = function()
			{
	
				$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';
	
				
				if ($scope.search.guests =="" && $scope.ItemId == 2)
				{
						$ionicPopup.alert({
						 title: 'יש להזין כמות מוזמנים',
						buttons: [{
							text: 'אשר',
							type: 'button-positive',
						  }]
					   });
				}

				
				else
				{
					//alert ($scope.CatagoryIndex)
					send_data = 
					{
						"catagory" : $scope.CatagoryIndex,
						"area" : $scope.search.area,
						"guests" : $scope.search.guests,
						"send" : 1
						
					}					
					$http.post($rootScope.Host+'/search_supplier.php',send_data).success(function(data)
					{
						console.log (data);
						$scope.SuppliersSearch = data[0].suppliers;
						if ($scope.SuppliersSearch.length == 0)
						{
							$scope.Suppliers = [];
							
							$ionicPopup.alert({
							 title: 'לא נמצאו תוצאות אנא נסה שנית',
							buttons: [{
								text: 'אשר',
								type: 'button-positive',
							  }]
						   });								
						}
						else 
						{
							$scope.Suppliers = data[0].suppliers;
							$scope.supplierSearchModal.hide();
						}
						//alert ($scope.SuppliersSearch.length)
						
					});						
					
					//$scope.supplierSearchModal.hide();
				}					
			}
	

	
	//	}, 300);
//	});
	
})

.controller('InfoCtrl', function($scope, $stateParams,$ionicPopup,$http,$rootScope,$state,$localStorage,$ionicSideMenuDelegate,$sce,$ionicScrollDelegate) 
{
	//$scope.$on('$ionicView.enter', function(e) 
  	//{
		//setTimeout(function()
		//{
			//alert ($stateParams.Favorite)
			//alert ($stateParams.ArticleId)
			$rootScope.ControllerName = "info";

			$scope.details = 
			{
				"name" : $localStorage.name,
				"phone" : "",
				"email" : $localStorage.email,
				"text" : ""
			}
			
			$scope.details.name = $localStorage.name;
			$scope.details.email =  $localStorage.email;

			
			//$scope.CatId = $stateParams.CatId;
			$scope.ArticleId = $stateParams.ArticleId;
			
				for(var i=0;i<$rootScope.Suppliers.length; i++)
				{
					if($rootScope.Suppliers[i].index == $scope.ArticleId)
					{
						$scope.Suppliers = $rootScope.Suppliers[i];
					}
				}
			
			//console.log('Data New: ', $rootScope.Data)	
			$scope.navTitle='<img class="title-image" src="img/logo.png" />'
			//$scope.Suppliers = $rootScope.Data;
			//$scope.Suppliers = $rootScope.Data[$scope.CatId].suppliers[$scope.ArticleId];
			//console.log($scope.Suppliers)
			$scope.StoryId = $scope.Suppliers.index;
			
			$scope.host = $rootScope.Host;
			$scope.CatagoryName = $rootScope.Catagoryname;
			$scope.Adress = $scope.Suppliers.address;
			$scope.Name = $scope.Suppliers.name;
			$scope.About = $scope.Suppliers.desc;
			$scope.SupplierImage= $scope.Suppliers.image2;
			$scope.SupplierImage2= $scope.Suppliers.image;
			$scope.Phone = $scope.Suppliers.phone;
			$scope.SupplierParams = $scope.Suppliers.supplierparams;
	
			
			$scope.emptyStar = 'img/suppliers/estar.png'
			$scope.fullStar = 'img/suppliers/estar.png'
			$scope.starImg = $scope.fullStar;
			$scope.hidedetails = 0;
			
			console.log("About")
			
			//$scope.About = $scope.About.replace('color="#333333"','');
			console.log($scope.About)
			//$scope.About = $sce.trustAsHtml($scope.About);
			//$scope.About = $scope.About.replace(/\//g,'');
			
			$scope.sendForm = function()
			{
	
				$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';
	
				
				if ($scope.details.name =="")
				{
						$ionicPopup.alert({
						 title: 'יש להזין שם מלא',
						buttons: [{
							text: 'אשר',
							type: 'button-positive',
						  }]
					   });
				}
				else if ($scope.details.phone =="")
				{
						$ionicPopup.alert({
						 title: 'יש להזין מספר טלפון',
						buttons: [{
							text: 'אשר',
							type: 'button-positive',
						  }]
					   });
				}
/*
				else if ($scope.details.email =="")
				{
						$ionicPopup.alert({
						 title: 'יש להזין כתובת מייל',
						buttons: [{
							text: 'אשר',
							type: 'button-positive',
						  }]
					   });
				}
				else if ($scope.details.text =="")
				{
						$ionicPopup.alert({
						 title: 'יש להזין תוכל הפנייה',
						buttons: [{
							text: 'אשר',
							type: 'button-positive',
						  }]
					   });
				}
*/				
				else
				{
					
					send_data = 
					{
						"id" : $scope.StoryId,
						"name" : $scope.details.name,
						"phone" : $scope.details.phone,
						"mail" : $scope.details.email,
						"text" : $scope.details.text,
						"isdeal" : 0,
						"send" : 1
						
					}					
					$http.post($rootScope.Host+'/send_offer.php',send_data).success(function(data)
					{
					});						
					
						$ionicPopup.alert({
						 title: 'תודה , פרטיך התקבלו בהצלחה נציג מטעם הספק יחזור אליך בהקדם האפשרי',
						buttons: [{
							text: 'אשר',
							type: 'button-positive',
						  }]
					   });					
					
					$scope.hidedetails = 0;
					
					
				}	
				
				
			}
			
			
			$scope.openChat = function()
			{
				$state.go('app.chat', { SupplierIndex: $scope.StoryId, UserId: 4 });
			}
			
			
			$scope.sendInfo = function()
			{
				$ionicScrollDelegate.scrollTop();
				$scope.hidedetails = 1;
				
			}
			
			$scope.closeDiv = function()
			{
				$scope.hidedetails = 0;
			}
				
  			$scope.PhoneSapak = function()
			{
				window.location.href="tel://"+$scope.Phone;
			}			
		//}, 300);
//	});
})


.controller('DealsCtrl', function($scope, $stateParams,$ionicPopup,$http,$rootScope,$state,$localStorage,$ionicSideMenuDelegate,$sce,$ionicModal) 
{
	//$scope.$on('$ionicView.enter', function(e) 
  //	{
//		setTimeout(function()
//		{
			$scope.host = $rootScope.Host;
			$scope.navTitle='<img class="title-image" src="img/logo.png" />'
			$scope.Deals = $rootScope.Deals;
			$rootScope.ControllerName = "deals";
			$scope.DealsSearchArray = new Array();
			$scope.SelectedPrice = 0;
			
			$scope.search = 
			{
				"area" : "",
				"guests" : "300",
				"season" : "",
				"catagory" : "4"
			}
			
			/*
			$scope.search.area
			$scope.search.guests
			$scope.search.season
			$scope.search.catagory
			*/

			$scope.dealSearch = function()
			{
			  $ionicModal.fromTemplateUrl('templates/deal_search.html', {
				scope: $scope
			  }).then(function(dealSearchModal) {
				$scope.dealSearchModal = dealSearchModal;
				$scope.dealSearchModal.show();
			  });
			}
			$scope.closeModal = function()
			{
				$scope.dealSearchModal.hide();
			}
	
			
			$scope.getSupplierByIndex = function(id)
			{
				for(var i=0;i<$rootScope.Suppliers.length; i++)
				{
					if($rootScope.Suppliers[i].index == id)
					{
						return $rootScope.Suppliers[i].name;
					}
				}
				
				DealsArray = new Array();
				
				for(var i=0;i<$rootScope.Deals.length; i++)
				{
					if($rootScope.Suppliers[i].index == id)
					{
						DealsArray = $rootScope.Deals[i];
					}
				}
				
			
			}
			

			
			//search modal filter
			$scope.FilterSearchDeal = function()
			{
				$rootScope.dealGuests = $scope.search.guests;
				
				
				$scope.DealsSearchArray = new Array();
				for(var i=0;i<$rootScope.Deals.length; i++)
				{
					//console.log("deals info ",$rootScope.Deals[i])
					if($rootScope.Deals[i].catagory == $scope.search.catagory && ($rootScope.Deals[i].area == $scope.search.area || $scope.search.area == "") && ($rootScope.Deals[i].season == $scope.search.season || $scope.search.season == ""))
					{
						if ($rootScope.Deals[i].catagory == 4 && parseInt($scope.search.guests) >= parseInt($rootScope.Deals[i].minsize) && parseInt($scope.search.guests) <= parseInt($rootScope.Deals[i].maxsize))
						{

							//alert ($rootScope.Deals[i].maxsize)
							$scope.SelectedPrice = $scope.getSelectedPrice($rootScope.Deals[i],$scope.search.guests);
							$scope.dealPrice = (parseInt($scope.search.guests)*parseInt($scope.SelectedPrice))+parseInt($rootScope.Deals[i].fixedprice);
							$rootScope.Deals[i].price = $scope.dealPrice;
							$rootScope.Deals[i].old_price = $scope.dealPrice+($scope.dealPrice*($rootScope.Deals[i].discount_precent/100));
							
							$rootScope.dealOldPrice = $rootScope.Deals[i].old_price;
							$rootScope.dealNewPrice = $rootScope.Deals[i].price;
							//console.log($scope.dealPrice);
							$scope.DealsSearchArray.push($rootScope.Deals[i]);
						}
						if ($rootScope.Deals[i].catagory != 4)
						{
							$scope.DealsSearchArray.push($rootScope.Deals[i]);
						}
						
							//console.log('new deals: ',$rootScope.Deals[i]);
								
							
					}
				}	
				
				console.log("deal 1 " , $scope.DealsSearchArray)
				
				if ($scope.DealsSearchArray.length == 0)
				{
					$ionicPopup.alert({
					 title: 'לא נמצאו תוצאות אנא נסה שנית',
					buttons: [{
						text: 'אשר',
						type: 'button-positive',
					  }]
				   });					
				}
				else if ($scope.dealSearchModal)
				{
					$scope.dealSearchModal.hide();						
				}
			
			}
			
			$scope.FilterSearchDeal();
			
			$scope.checkPercent = function(deal)
			{
				z=deal.old_price-deal.price;
				z= z/deal.old_price;
				z = parseInt(z*100);
				//alert(parseInt(deal.price)*parseInt(deal.discount_precent/10))
				return z;
			}
			
			$scope.cutDate = function(date)
			{
				var dArray = date.split('/');
				dateStr = dArray[0]+"."+dArray[1]
				return dateStr;
			}
			
			$scope.cutYear = function(date)
			{
				var dArray = date.split('/');
				dateStr = dArray[2]
				return dateStr
			}
			
			$scope.navigateDeal = function(id,oldprice,newprice)
			{
				$rootScope.dealOldPrice = oldprice;
				$rootScope.dealNewPrice = newprice;		
				$state.go('app.dealsInfo', { ItemId: id });
			}
	
			
//		}, 300);
	//});
})

.controller('dealsInfoCtrl', function($scope, $stateParams,$ionicPopup,$http,$rootScope,$state,$localStorage,$ionicSideMenuDelegate,$sce,$ionicScrollDelegate) 
{
	//$scope.$on('$ionicView.enter', function(e) 
  //	{
//		setTimeout(function()
//		{
			$scope.host = $rootScope.Host;
			$scope.Deal = [];
			$scope.Supplier = [];
			$rootScope.ControllerName = "dealsinfo";
			$scope.dealGuests = $rootScope.dealGuests;
			$scope.dealNewPrice = $rootScope.dealNewPrice;
			$scope.dealOldPrice = $rootScope.dealOldPrice;			
			

			$scope.details = 
			{
				"name" : $localStorage.name,
				"phone" : "",
				"email" : $localStorage.email,
				"text" : ""
			}

			
			for(var i=0;i<$rootScope.Deals.length;i++)
			{
				if( $rootScope.Deals[i].index == $stateParams.ItemId)
				{
					$scope.Deal = $rootScope.Deals[i];
				}
			}
			
			for(var i=0;i<$rootScope.Suppliers.length;i++)
			{
				if($rootScope.Suppliers[i].index == $scope.Deal.supplier_index)
				{
					$scope.Supplier = $rootScope.Suppliers[i];
					//console.log("SupplierInfo : " , $scope.Supplier.desc)
				}
			}
			
			console.log("Supplier: ",$scope.Supplier)
			console.log("About");
			console.log($scope.Deal);
			$scope.hidedetails = 0;
			$scope.DealIndex = $scope.Deal.index;
			$scope.SupplierId = $scope.Supplier.index;
			$scope.Phone = $scope.Supplier.phone;
			
			//console.log($scope.Supplier)
			
			$scope.getCategory = function()
			{
				for(var i=0;i<$rootScope.Data.length;i++)
				{
					if($scope.Supplier.category == $rootScope.Data[i].index)
					return $rootScope.Data[i].name;
				}
			}
			
			$scope.cutDate = function(date)
			{
				var dArray = date.split('/');
				dateStr = dArray[0]+"."+dArray[1]
				return dateStr;
			}
			
			$scope.cutYear = function(date)
			{
				var dArray = date.split('/');
				dateStr = dArray[2]
				return dateStr
			}

			$scope.sendForm = function()
			{
	
				$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';
	
				
				if ($scope.details.name =="")
				{
						$ionicPopup.alert({
						 title: 'יש להזין שם מלא',
						buttons: [{
							text: 'אשר',
							type: 'button-positive',
						  }]
					   });
				}
				else if ($scope.details.phone =="")
				{
						$ionicPopup.alert({
						 title: 'יש להזין מספר טלפון',
						buttons: [{
							text: 'אשר',
							type: 'button-positive',
						  }]
					   });
				}
			/*
				else if ($scope.details.email =="")
				{
						$ionicPopup.alert({
						 title: 'יש להזין כתובת מייל',
						buttons: [{
							text: 'אשר',
							type: 'button-positive',
						  }]
					   });
				}
				else if ($scope.details.text =="")
				{
						$ionicPopup.alert({
						 title: 'יש להזין תוכל הפנייה',
						buttons: [{
							text: 'אשר',
							type: 'button-positive',
						  }]
					   });
				}
			*/
				else
				{
					
					send_data = 
					{
						//"id" : $scope.DealIndex,
						"id" : $scope.SupplierId,
						"deal_id" : $scope.DealIndex,
						"name" : $scope.details.name,
						"phone" : $scope.details.phone,
						"mail" : $scope.details.email,
						"text" : $scope.details.text,
						"isdeal" : 1,
						"send" : 1
						
					}					
					$http.post($rootScope.Host+'/send_offer.php',send_data).success(function(data)
					{
					});						
					
						$ionicPopup.alert({
						 title: 'תודה , פרטיך התקבלו בהצלחה נציג מטעם הספק יחזור אליך בהקדם האפשרי',
						buttons: [{
							text: 'אשר',
							type: 'button-positive',
						  }]
					   });					
					
					$scope.hidedetails = 0;
					
					
				}	
				
				
			}
			
			
			$scope.sendInfo = function()
			{
				$ionicScrollDelegate.scrollTop();
				$scope.hidedetails = 1;
				
			}
			
			$scope.closeDiv = function()
			{
				$scope.hidedetails = 0;
			}
			
			$scope.openChat = function()
			{
				$state.go('app.chat', { SupplierIndex: $scope.SupplierId, UserId: 4 });
			}
			
			$scope.PhoneDeal = function()
			{
				window.location.href="tel://"+$scope.Phone;

			}
			
			$scope.AddFavorite= function()
			{
				
				send_params = 
				{
						"user" : $localStorage.userid,
						"article" : $scope.DealIndex,
						"is_deal" : 1
				}
				//console.log(chat_params)
				$http.post($rootScope.Host+'/add_favorite.php',send_params)
				.success(function(data, status, headers, config)
				{
					$ionicPopup.alert({
					 title: 'נוסף בהצלחה למועדפים',
					buttons: [{
						text: 'אשר',
						type: 'button-positive',
					  }]
				   });
				})
				.error(function(data, status, headers, config)
				{

				});				
				
			}

	
				
			/*$scope.navTitle='<img class="title-image" src="img/logo.png" />'
			$scope.Deals = $rootScope.Data;
			$scope.Supllier = $rootScope.Suppliers
			
			$scope.host = $rootScope.Host;
			$scope.Id = $stateParams.ItemId;
			$scope.Adress = $scope.Supplier.address;
			$scope.Name = $scope.Supplier.name;
			$scope.About = $scope.Deal.desc;
			$scope.emptyStar = 'img/suppliers/estar.png'
			$scope.fullStar = 'img/suppliers/estar.png'
			$scope.starImg = $scope.fullStar;
			
			console.log("About")
			console.log($scope.Suppliers)
			//$scope.About = $scope.About.replace('color="#333333"','');
			console.log($scope.About)
			//$scope.About = $sce.trustAsHtml($scope.About);
			//$scope.About = $scope.About.replace(/\//g,'');*/
			
  			$scope.serachSupplier = function()
			{
				
			}
			
//		}, 300);
	//});
})


.controller('blogCtrl', function($scope, $stateParams,$ionicPopup,$http,$rootScope,$state,$localStorage,$ionicSideMenuDelegate,$sce) 
{
	//$scope.$on('$ionicView.enter', function(e) 
  //	{
	//	setTimeout(function()
		//{
			$scope.navTitle='<img class="title-image" src="img/logo.png" />'
		//}, 300);
	//});
	$scope.ImgHost = $rootScope.Host;
	$scope.Blog = $rootScope.BlogPosts;
	$rootScope.ControllerName = "blog";

	
	$scope.navigateUrl = function (Path,Num) 
	{
		window.location.href = Path+String(Num);
	}
		
	
})


.controller('articleCtrl', function($scope, $stateParams,$ionicPopup,$http,$rootScope,$state,$localStorage,$ionicSideMenuDelegate,$sce) 
{
	//$scope.$on('$ionicView.enter', function(e) 
  //	{
		//setTimeout(function()
		//{
			$scope.navTitle='<img class="title-image" src="img/logo.png" />'
			$scope.InfoAboutText = "";
			$rootScope.ControllerName = "article";

		//}, 300);
	//});
	
	$scope.Id = $stateParams.ItemId;
	$scope.ImgHost = $rootScope.Host;
	$scope.BlogPost = $rootScope.BlogPosts[$scope.Id];

	
})

.controller('ContactCtrl', function($scope, $stateParams,$ionicPopup,$http,$rootScope,$state,$localStorage,$ionicSideMenuDelegate,$sce) 
{
	
	$rootScope.ControllerName = "contact";

	$scope.contact = 
	{
		"name" : $localStorage.name,
		"email" :  $localStorage.email,
		"phone" : "",
		"details" : ""
	}
	
	$scope.SendContact = function()
	{

	$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';
    emailRegex = /\S+@\S+\.\S+/;
    

	if ($scope.contact.name =="")
	{
			$ionicPopup.alert({
			 title: 'יש להזין שם מלא',
			buttons: [{
				text: 'אשר',
				type: 'button-positive',
			  }]
		   });
	}
	
	else if ($scope.contact.email =="" && $scope.contact.phone =="")
	{
			$ionicPopup.alert({
			 title: 'יש להזין כתובת דוא"ל או טלפון',
			buttons: [{
				text: 'אשר',
				type: 'button-positive',
			  }]
		   });
	}
	else if ($scope.contact.details =="")
	{
			$ionicPopup.alert({
			 title: 'יש להזין תוכן הפנייה',
			buttons: [{
				text: 'אשר',
				type: 'button-positive',
			  }]
		   });
	}	
	else
	{
		send_data = 
		{
			"user" : $localStorage.userid,
			"name" : $scope.contact.name,
			"email" : $scope.contact.email,
			"phone" : $scope.contact.phone,
			"details" : $scope.contact.details,
			"send" : 1
		}					
		$http.post($rootScope.Host+'/contact.php',send_data).success(function(data)
		{
		});		
		

			$ionicPopup.alert({
			 title: 'תודה , פרטיך התקבלו בהצלחה נחזור אליך בהקדם האפשרי',
			buttons: [{
				text: 'אשר',
				type: 'button-positive',
			  }]
		   });
		   
		$state.go('app.main');
	}
	}
	


})


.controller('FavoritesCtrl', function($scope, $stateParams,$ionicPopup,$http,$rootScope,$state,$localStorage,$ionicSideMenuDelegate,$sce,$ionicLoading) 
{
//$scope.$on('$ionicView.enter', function(e) {

    $ionicLoading.show({
      template: 'טוען...<ion-spinner icon="spiral"></ion-spinner>'
    });
	
	$scope.NewDealsFavorites = new Array();
	
	
	$scope.getFavorites = function()
	{
		params = 
		{
				"user" : $localStorage.userid
		}
			
		$http.post($rootScope.Host+'/getFavorites.php',params).success(function(data)
		{
			$ionicLoading.hide();
			$scope.Favorites = data;
			$scope.FavoriteSuppliers = $scope.Favorites.suppliers;
			$scope.FavoriteDeals = $scope.Favorites.deals;
			console.log('favorites:' , data);
			console.log('favorites deals: ' , $scope.FavoriteDeals);
			$scope.NewDealsFavorites = new Array();
			for(var i=0;i<$scope.FavoriteDeals.length; i++)
			{
				//if($scope.FavoriteDeals[i].catagory == $scope.search.catagory && ($scope.FavoriteDeals[i].area == $scope.search.area || $scope.search.area == "") && ($rootScope.Deals[i].season == $scope.search.season || $scope.search.season == ""))
				//{
					if ($scope.FavoriteDeals[i][0].catagory == 4)
					{
						$scope.SelectedPrice = $scope.getSelectedPrice($scope.FavoriteDeals[i][0],300);
						$scope.dealPrice = (parseInt(300)*parseInt($scope.SelectedPrice))+parseInt($scope.FavoriteDeals[i][0].fixedprice);
						$scope.FavoriteDeals[i][0].price = $scope.dealPrice;
						$scope.FavoriteDeals[i][0].old_price = $scope.dealPrice+($scope.dealPrice*($scope.FavoriteDeals[i][0].discount_precent/100));
						
						//$rootScope.dealOldPrice = $rootScope.Deals[i].old_price;
						//$rootScope.dealNewPrice = $rootScope.Deals[i].price;
						//console.log($scope.FavoriteDeals[i][0].old_price)
						//console.log($scope.FavoriteDeals[i][0].price)
					}
						//console.log('new deals: ',$rootScope.Deals[i]);
						$scope.NewDealsFavorites.push($scope.FavoriteDeals[i]);	
			//	}
			}			
		});		
	}
	
	$scope.getFavorites();
	
	$scope.navigateDeal = function(id,oldprice,newprice)
	{
		$rootScope.dealOldPrice = oldprice;
		$rootScope.dealNewPrice = newprice;		
		$state.go('app.dealsInfo', { ItemId: id });
	}
  
	$scope.navTitle='<img class="title-image" src="img/logo.png" />'
	$rootScope.ControllerName = "favorites";
	$scope.host = $rootScope.Host;
	$scope.emptyStar = 'img/suppliers/estar.png'
	$scope.fullStar = 'img/suppliers/fstar.png'
	$scope.starImg = $scope.fullStar;

	
	//console.log($scope.FavoriteDeals)
	
	
	$scope.removeFavorite = function(index,id,isDeal)
	{
	   var confirmPopup = $ionicPopup.confirm({
		 title: 'האם לאשר מחיקה?',
		  buttons: [{ 
			text: 'אישור',
			type: 'button-positive',
			onTap: function(e) {
			
			
			$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';

			send_params = 
			{
					"user" : $localStorage.userid,
					"article" : id,
					"deal" : isDeal

			}
				//console.log(chat_params)
				$http.post($rootScope.Host+'/delete_favorites.php',send_params)
				.success(function(data, status, headers, config)
				{
				})
				.error(function(data, status, headers, config)
				{

				});	

				
				$scope.FavoriteSuppliers.splice(index, 1);
			}
		  }, {
			text: 'ביטול',
			type: 'button-default',
			onTap: function(e) {
			}
		  }]		 
	   });
	}
	
	$scope.checkNewPrice = function(deal)
	{
		//alert(parseInt(deal.price)*parseInt(deal.discount_precent/10))
		return parseInt(deal.price)*parseInt(deal.discount_precent/10)
	}
	
	$scope.cutDate = function(date)
	{
		var dArray = date.split('/');
		dateStr = dArray[0]+"."+dArray[1]
		return dateStr;
	}
	
	$scope.cutYear = function(date)
	{
		var dArray = date.split('/');
		dateStr = dArray[2]
		return dateStr
	}	
	
//});
})	


.controller('ChatCtrl', function($scope, $stateParams,$ionicPopup,$http,$rootScope,$state,$localStorage,$ionicSideMenuDelegate,$sce,$ionicScrollDelegate,$timeout,chatHistoryFactory) 
{
	
//	$scope.$on('$ionicView.enter', function(e) 
//{
//	setTimeout(function()
//	{


	$rootScope.ControllerName = "chat";
	
	var channel = $rootScope.pusher.subscribe('wedx_messages');
    channel.bind($localStorage.userid, function(data) {
	$timeout(function() { 	
	  if (data.status)
	  {
		 $scope.getChatHistory();	
	  }
	}, 1000);
    });
	
	
	
	$scope.SupplierIndex = $stateParams.SupplierIndex;
	$scope.SenderId  = $stateParams.UserId;
	$scope.ReciverId  = $stateParams.UserId;
	
	//$scope.CatIndex = $stateParams.CatIndex;
	$scope.imagePath = $rootScope.Host;
	$scope.navTitle='<img class="title-image" src="img/logo.png" />';

	//alert ($scope.SupplierIndex)
	//alert ($scope.SenderId)


	$ionicScrollDelegate.scrollBottom();
	$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';
	$scope.chatArray = new Array();
	$scope.user_local_storage = $localStorage.userid;
	
	$scope.chatFields = 
	{
		"chatbox" : ""	
	};


	
	for(var i=0;i<$rootScope.Suppliers.length;i++)
	{
		if($scope.SupplierIndex == $rootScope.Suppliers[i].index)
		{
			$scope.SupplierDetails = $rootScope.Suppliers[i];
		}
	}	
		
	
		$scope.userid = $scope.SupplierDetails.userids[0];
	
		//$scope.SupplierDetails =  $rootScope.Data[$scope.CatIndex].suppliers[$scope.SupplierIndex];
		$scope.SupplierImage = $scope.SupplierDetails.image;
		$scope.SupplierName = $scope.SupplierDetails.name;
		$scope.SupplierIndex = $scope.SupplierDetails.index;
		$scope.SupplierEmail = $scope.SupplierDetails.email;
		$rootScope.SupplierDetails = $scope.SupplierDetails;
		
		


		
	//get chat messages
	
	$scope.getChatHistory = function() 
	{
		chatHistoryFactory.getHistory($localStorage.userid,$scope.SenderId).then(function(data)
		{
			if (data.length > 0)
			{
				$scope.chatArray = data;
				console.log($scope.chatArray);		
			}
	
			$timeout(function() { $ionicScrollDelegate.scrollBottom();}, 1000);
		});
	}
	
	$scope.getChatHistory();

	
	$scope.sendChat = function()
	{
		if ($scope.chatFields.chatbox)
		{
			//console.log($scope.chatArray);
			chat_params = 
			{
					"user" : $localStorage.userid,
					//"recipent" : $scope.SupplierIndex,
					"recipent" : $scope.ReciverId,
					"message" : $scope.chatFields.chatbox,
					"username" :  $localStorage.name,
					"image" : $localStorage.image,
					"supplier_index" : $scope.SupplierIndex
			}
				//console.log(chat_params)
				$http.post($rootScope.Host+'/send_chat_message.php',chat_params)
				.success(function(data, status, headers, config)
				{
					$scope.date = new Date()
					$scope.hours = $scope.date.getHours()
					$scope.minutes = $scope.date.getMinutes()
				
					if ($scope.hours < 10)
					$scope.hours = " " + $scope.hours
					
					if ($scope.minutes < 10)
					$scope.minutes = "0" + $scope.minutes
				
					$scope.time = $scope.hours+':'+$scope.minutes;
					
					$scope.chat  = {
						"username" : $localStorage.username,
						"text" : $scope.chatFields.chatbox,
						"image" : $localStorage.image,
						"userid" : $localStorage.userid,
						"time" : $scope.time
					}
	
					console.log("chatArray")
					console.log($scope.chatArray)
					$scope.chatArray.push($scope.chat);
					$scope.chatFields.chatbox = '';
					$ionicScrollDelegate.scrollBottom();
				})
				.error(function(data, status, headers, config)
				{

				});	



					
		}
	}

	
	$scope.enterPress = function(keyEvent)
	{
		if (keyEvent.which === 13)
		{
			$scope.sendChat();
		}
	}
	
	
//	}, 300);
	
//});	
		

})


.controller('MessagesCtrl', function($scope, $stateParams,$ionicPopup,$http,$rootScope,$state,$localStorage,$ionicSideMenuDelegate,$sce,$ionicHistory) 
{
	//$scope.$on('$ionicView.enter', function(e) {
		
	$rootScope.ControllerName = "messages";

	
	$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';

				
	send_data = {"user" : $localStorage.userid	}		
	
	$http.post($rootScope.Host+'/get_messages.php',send_data).success(function(data)
	{
		$scope.messagesArray = data.messages.response;
		console.log("MessagesCntrl",$scope.messagesArray)
	});	
					
					
	$scope.searchMessages = function(value)
	{
		return true;
		/*
		if (value.username_id != $localStorage.userid)
		{
			return true;
		}
		*/
	}
	
	$scope.goToChat = function(Message)
	{
		$scope.ChatId = "";
		if(Message.recipent_id == $localStorage.userid)
		$scope.ChatId = Message.sender_id;
		else
		$scope.ChatId = Message.recipent_id;
		
		$ionicHistory.nextViewOptions({
			disableAnimate: true
		});
		
		$ionicSideMenuDelegate.toggleRight();
		window.location.href = "#/app/chat/"+Message.supplier_index+"/"+$scope.ChatId;
	}
	
	
	
//	});
	
})	



.filter('cutString_TitlePage', function () {
    return function (value, wordwise, max, tail) 
	{
		value =  value.replace(/(<([^>]+)>)/ig,"");
        if (!value) return '';
		
        max = parseInt(max, 10);
        if (!max) return value;
        if (value.length <= max) return value;

        value = value.substr(0, max);
    //    if (wordwise) {
            var lastspace = value.lastIndexOf(' ');
            if (lastspace != -1) {
                value = value.substr(0, lastspace);
            }
     //   }
        return value + (tail || ' ');
    };
})

.filter('stripslashes', function () {
    return function (value) 
	{
	    return (value + '')
		.replace(/\\(.?)/g, function(s, n1) {
		  switch (n1) {
			case '\\':
			  return '\\';
			case '0':
			  return '\u0000';
			case '':
			  return '';
			default:
			  return n1;
		  }
		});
    };
})


.filter('checkImgUrl', function ($rootScope,$localStorage) {
    return function (value) 
	{
		var Img = "";
		
		if ($rootScope.SupplierDetails.email == $rootScope.Suppliers[value.userid-1].email)
		{
			Img = $rootScope.Host + $rootScope.SupplierDetails.image;
		}
		else if(value)
		{
			if(value.image.charAt(0) == "u")
			{
				Img = $rootScope.Host + "/"+value;
			}
			else
			{
				Img = "img/chat/avatar.gif";
			}	
		}
		else
		{
			Img = "img/chat/avatar.gif";
		}
	
	
		console.log(Img)
		return Img;
		
    };
	
})

.filter('lastseenDate', function ($rootScope) {
    return function (value) 
	{
		if(value != "s")
		{
			var splitLastSeen = '';
		
			if (value.lastLogin)
			splitLastSeen =  value.lastLogin.split(" ");
			else if (value.date)
			splitLastSeen =  value.date.split(" ");

			var date = splitLastSeen[0];
			var hour = splitLastSeen[1];
		
			var splitdate =  date.split("-");
			var splithour =  hour.split(":");
		
			return ''+splitdate[2]+'/'+splitdate[1]+'/'+splitdate[0]+' '+ splithour[0]+':'+splithour[1]+'';
		}
		
    };
})




