angular.module('starter.factories', [])


.factory('chatHistoryFactory', function($http,$rootScope) {
	var data = [];

	return {
		getHistory: function(user,recipent){
			
			$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';
			
			data_params = 
			{
				"user" : user,
				"recipent" : recipent
			}
			
			return $http.post($rootScope.Host+'/get_chat_history.php',data_params).then(function(resp)
			{
				console.log("History",resp)
				data = resp.data.response;
				//return users;
				console.log ("Messages",data);
				return data;
			});
		},
		/*
		getUser: function(id){
			for(i=0;i<users.length;i++){
				if(users[i].id == id){
					return users[i];
				}
			}
			return null;
		}
		*/
	}
})

.factory('getSupplierByName', function($http,$rootScope) {
	var data = [];

	return {
		getHistory: function(user,recipent){
			
			$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';
			
			data_params = 
			{
				"user" : user,
				"recipent" : recipent
			}
			
			return $http.post($rootScope.Host+'/get_chat_history.php',data_params).then(function(resp)
			{
				console.log("History",resp)
				data = resp.data.response;
				//return users;
				console.log ("Messages",data);
				return data;
			});
		},
		/*
		getUser: function(id){
			for(i=0;i<users.length;i++){
				if(users[i].id == id){
					return users[i];
				}
			}
			return null;
		}
		*/
	}
})

